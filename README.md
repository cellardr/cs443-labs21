# Databases Labs at Esisar :school:

Student (public) files

Academic year 2021-22

Laure Gonnord

mailto `Laure.Gonnord@esisar.grenoble-inp.fr`


## Cours :book: et TD :pencil:

Sur [Chamilo/CS443](https://chamilo.grenoble-inp.fr/courses/ESISAR5AMCS443/index.php)


## TP :computer:

* [TP1 (2 séances) Le Tableur c'est le maaaal](TP01/README.md)

* [TP2 (en autonomie + rendu) requêtes SQL](TP-SQL/README.md)

* [TP3 (1 séance + rendu) Le Tableur c'est le maaaal](TP03/README.md)

