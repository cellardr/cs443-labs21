# CS 443 (BD) TP SQL en autonomie

  * Laure Gonnord
  * Version: 2021.01
  * Source/Inspiration : les collègues Lillois (Polytech'Lille) :purple_heart:
  * :watch: en autonomie
  * Rendu :pencil: sur Chamilo avant le 17/12/2021 18H (unique `nomprenom.sql` sur Chamilo, commenté)

## Objectif

L'objectif de ce TP est de réaliser des requêtes `mysql` sur une base de données simple, déjà vue en TD. Idéalement, ce TP serait réalisé individuellement. J'autorise néanmoins les rendus en binôme.

## Consignes.

* Vous utiliserez sqlite3 et le logiciel `sqlitebrowser`, dont on aura fait une démo en TD.
* Une documentation SQL est disponible sur Chamilo.
* Faire simple et expliquer en commentaire les requêtes complexes.
* La base de donnée et sa construction sont fournies dans le répertoire `_code`
* Certaines requêtes ont déjà été réalisées en TD.

## Requêtes à réaliser.

Vous utiliserez `sqlitebrower` pour essayer vos requêtes (avec le mode pas à pas). Le fichier de rendu "squelette" est présent dans le répertoire `code`. 

* Tuples de la table 'film'.
* Nom et Prénom des abonnés. On affiche le résultat sur une colonne contenant le nom et le prénom. Pour cela, on a besoin de  la concaténation sur les chaînes (voir le cours). On affichera tout en majuscule (opérateur `upper`). Trouver un libellé signifiant pour la colonne.
* Titres des films antérieurs à 2005.
* Titres des films parus entre 2005 et 2010, triés par ordre alphabétique.
* Film(s) dont le titre commence par 'p'.
* Film(s) dont le titre contient 'se'
* Libellé(s) des catégories dont font partie les films
 présents dans la base. (Éviter les doublons)
* Nombre de catégories différentes dont font partie les films présents dans la base. On donnera un libellé signifiant au  résultat.
* Nombre de films par catégorie.
* Nombre moyen d'emprunts par abonné (2 requêtes).
* Titres des films classés dans la catégorie 'Drame'
* Titres et années des films de Tim Burton.
* Nombre de films réalisés par Clint Eastwood.
* Numéro des films de Woody Allen avec pour chaque film, le nombre de dvds correspondants.
* Titre du film dont on possède le plus de dvds.
* Numéro des films dont on possède plusieurs dvd dans la base.
* Numéro des films avec le nombre de dvd correspondants qu'on possède.
* Noms et prénoms des abonnés ayant emprunté 'Pulp Fiction'.
* Liste des films des catégories 'Drame' et 'Action'.
* Liste des films empruntés par Marie Dupont.
* Liste des films n'ayant jamais été empruntés.
* Titre des films de Woody Allen de la categorie 'Drame'.
* Liste des films dont au moins un dvd a été acheté en août 2009.
* Noms et prénoms des abonnés ayant emprunté un film de  Woody Allen et un film de Clint Eastwood.
* Liste des films ayant été empruntés par tous les abonnés.
* Liste des films dont l'année est antérieure aux années des films réalisés par Tim Burton.
* Liste des films dont on ne possède pas de dvd.
* Liste des abonnés ayant empruntés tous les films réalisés par Quentin Tarantino.
* Titre des films produits en 2008 ou 2009.
* Liste des films dont au moins un dvd a été acheté l'année de sa production.
* Titre des films réalisés ni en 2007, ni en 2008, ni en 2009. 
