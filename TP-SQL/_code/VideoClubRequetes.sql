----------------------------------------------------------------------
-- REQUETES TP Vidéoclub
-- 2021, Esisar IR&C CS 443


-- 1. Tuples de la table ’film’
select * from film;


-- 2. Nom et Prénom des abonnés.
select upper(nomab || ' ' || prenomab) as les_abonnes from abonne ;

-- 3. Titres des film antérieures à 2005.
select titre from film where anneeProduction < 2005;

-- 4. Titres des film parus entre 2005 et 2010, triés par ordre alphabétique.
