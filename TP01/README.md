# CS 443 (BD) Pourquoi des bases de données ? 

  * Laure Gonnord
  * Version: 2021.01
  * Source/Inspiration : les collègues Lillois (Polytech'Lille) :purple_heart:
  * :watch: Deux séances : 3H.
  * Rendu :pencil: notes personnelles, qui seront à utiliser plus tard (pas de rendu officiel sur Chamilo)

## Objectif

L'objectif de ce TP est de justifier l'usage des bases de données par des petites manipulations d'une table, qui seront quelquefois fastidieuses et quelquefois non adaptées. 


## On démarre (sous Linux)!

* Après le projet de Compilation (CS444), vous ne devez plus êtes perdu·e·s avec git :-) ** attention ** ce git est uniquement en lecture pour vous. 

* Les installations préliminaires aux TPs sont toujours expliquées dans un fichier séparé, aujourdhui il n'y a pas d'installation préliminaire de logiciel, car nous allons utiliser LibreOffice.

* Vous pouvez bien sûr utiliser vos machines personnelles.

* Vous pouvez réaliser ce TP en binôme.


## Partie Tableur

Le fichier `BDStages.ods` (répertoire `_files/`) contient des informations sur des départs à l'étranger (ie hors France) des étudiant·e·s de l'école Polytech'Lille. Ouvrez ce fichier avec LibreOffice et sauvegardez une version personnelle.


Questions:
* Observer le fichier fourni et comprendre quelles données sont stockées et comment. À votre avis, comment ce fichier a-t-il été obtenu ?
* Comment réaliser une version conforme à la RGPD de ce fichier? Que faut-il garantir ? (recherche web). Pour information, j'ai anonymisé les noms et prénoms à l'aide du [site de générateur de données](https://www.onlinedatagenerator.com/)

Questions "fonctionnelles":
Il est demandé de travailler au maximum avec des façons génériques de récupérer ces informations: filtres, tris, et aussi des fonctions (notamment des fonctions booléennes); ne pas oublier "count" pour dénombrer. 

*   Quel·le·s étudiant·e·s sont parti·e·s à  l'étranger pour leur stage découverte en entreprise ?
*   Combien d'étudiant·e·s sont parti·e·s en Chine en 2012 ?
*   Quel est le nom et le prénom des étudiant·e·s parti·e·s plusieurs fois à l'étranger ?
*   Combien d'étudiant·e·s sont parti·e·s plusieurs fois à l'étranger ?
*   Où a été Remi Hope ?
*   Quels sont les organismes américains qui ont reçu des étudiant·e·s de Polytech ?
*   Quel est le nom et le pays des entreprises qui ont reçu plusieurs étudiant·e·s ?
*   Quelle est la rémunération moyenne des étudiant·e·s ?
*   Quel·le·s sont les étudiant·e·s qui sont partis en pays anglophones (c'est-à-dire USA, Australie, Irlande, Angleterre, Canada) ?
*   Quel est le département qui a envoyé le plus d'étudiant·e·s à l'étranger ?
*   Quel est le sujet de stage de Charlotte Logan ?
*   Quels sont les contacts de l'INRS ?
*   Sur quelle période, Justine Lomax a-t-elle effectué son stage ?
*   Quels ont été les lieux et les sujets des stages des étudiant·e·s partis en Pologne ?
 
Questions "bilan":
* Comment sont gérées d'éventuelles personnes homonymes ? 
* Comment avez-vous traité le cas des majuscules vs minuscules dans vos recherches ?
* Comment avez-vous réalisé les interrogations "combien d'étudiant·e·s... "?
* Écrivez les difficultés que vous avez rencontrées.

## Partie Python : ménage et traitement automatique.

On reprend le fichier initial, que l'on sauvegarde (via LibreOffice)
en `.csv`, et on va un peu scripter en Python pour préparer des
manipulations futures en TP.

On vous fournit dans `_code/PythonCSV` un programme Python pour lire les données d'un fichier csv.

* Observez les fonctions fournies.
* Proposez et implémentez une solution Python au problème de doublon des noms de Pays.
* Proposez et implémentez  une solution Python au problème de mauvaise gestion prénom/nom. Pensez en terme d'identifiant unique.
* Réalisez des fonctions Python pour récupérer les réponses précédentes. 

## Partie BD

* Proposez une version normalisée du fichier initial qui pourra servir à construire une "vraie base de données"
* Une ref qui peut être utile : [pays/SQL](https://sql.sh/514-liste-pays-csv-xml)

* Proposez un schéma entité/association qui capture le maximum des relations de ce fichier.
