#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Example of script to get csv file information,
cf https://docs.python.org/fr/3/library/csv.html
"""
__author__ = "Laure Gonnord"
__copyright__ = "Grenoble INP/Esisar, 2021"

import csv

def get_n_print(filename, fieldnames):
    """
    Use csv lib to get info from file
    """
    with open(filename, newline='') as csvfile:
        reader = csv.DictReader(csvfile, fieldnames=fieldnames)
    # Lire la doc de la librairie pour récupérer automatiquement les noms des champs de la "première ligne" du fichier
        for row in reader:
            print(row['x'], row['y'])
    return 0


if __name__ == "__main__":
    # Pourrait être demandé à l'utilisateur, oui!
    get_n_print('donnees-2-colonnes.csv',  ['x', 'y'])
