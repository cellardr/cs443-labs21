# CS 443 (BD) TP 3 de synthèse

  * Laure Gonnord
  * Version: 2021.01
  * Source/Inspiration : les collègues Lillois (Polytech'Lille) :purple_heart:
  * :watch: 1 séance : 1h30
  * Rendu : synthèse sur Chamilo (5 pages max). Deadline TBA
  * Vous pouvez réaliser ce TP en binôme.

## Objectif

L'objectif de ce TP est de réaliser quelques traitements sur la base de données étudiante que l'on a travaillée au TP1. C'est l'occasion de retravailler des requêtes sur une BD un peu plus grosse que la BD jouet du TP en autonomie.

Remarque: on privilégiera toujours les jointures explicites:
```
SELECT * FROM etudiant
INNER JOIN stage on stage.etudiant = etudiant.numEt 
WHERE formation=3 ;
```
plutôt que les bonnes vieilles requêtes un peu fouillis:
```
SELECT * FROM etudiant,stage
WHERE formation=3 and ...
```

Le compte rendu de TP ne détaillera pas la conception des requêtes, mais réalisera quelques comparaisons bien choisies sur 5 questions du TP1, entre les versions tableur et la version BD.


## On démarre (sous Linux)!

* Même configuration que le TP en autonomie

* on fournit dans `_code` un script SQL de création d'une BD, et un autre de peuplement de cette bd. IL est recommandé de faire "import" pour la création, et d'utiliser l'onglet "exécuter le SQL" pour l'autre fichier. **Attention** il est normal d'obtenir des erreurs lors de la création de la base.



## Création et remplissage de la base, première requête

* Comprendre les mots clés de création de chacune des tables `relationsEtudiants.sql` (lire la doc!), et obtenir:
![Les tables dans sqlitebrower](figs/structure_bd.png)
(expliquer un peu dans le rapport)

* Comprendre le peuplement de la base et les erreurs obtenues. Dans un premier temps vous pouvez commenter les lignes qui provoquent des erreurs d'insertion. (Il est demandé d'expliquer dans le rapport une solution à ce problème, avec ses avantages et ses inconvénients).

* Les requêtes seront effectuées dans un nouveau fichier sql dans un nouvel onglet:

![Un exemple de jointure](figs/jointure.png)

à ce stade, vous devriez pouvoir réaliser des requêtes simples.

**ATTENTION** à la sauvegarde de vos requêtes, control-S ne fait pas la sauvegarde voulue.

## Requêtes

* Voir le TP1. Après avoir réalisé au maximum ces requêtes, il est demandé d'effectuer une comparaison argumentée des solutions trouvées tableur/bd pour ces requêtes. (2 ou 3 pages max dans le rapport)

## Peuplement de la table.

* On vous a fourni un fichier `donneesEtudiants.sql` construit à partir du csv précédent du TP1. Quelles opérations ont été réalisées, à votre avis? Détailler sur un point précis (gestion des pays, par exemple). (1 page max)

## (Bonus)

* Livrez un script python qui réalise la génération d'un tel fichier sql de peuplement de la base (voire de création) à partir du csv du TP1.

## (Malus)

* Il est recommandé de passer un correcteur orthographique et grammatical, un malus sera appliqué en cas de syntaxe défaillante.
